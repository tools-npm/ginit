# ginit-cli
Studying command line application with nodejs

## Source article available at [sitepoint](https://www.sitepoint.com/javascript-command-line-interface-cli-node-js/)


### Run:
```console
npm install  
npm start  
```
